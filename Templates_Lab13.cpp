/*=============================================================================
Name: 		Eduardo Ortiz
e-mail:		eduardo3991@hotmail.com
=============================================================================*/

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

void test_median();
void test_inOrder();
void test_at();
void test_myPair_operator();

template <typename Type>
Type median(vector<Type> V)
{
	sort(V.begin(), V.end());

	if(V.size() % 2 == 1)
		return V[V.size()/2];
	else
		return (V[V.size()/2] + V[V.size()/2 + 1]) / 2;
}

template <typename Type>
bool inOrder(Type array[], int size)
{
	if(size == 0 or size == 1) return true;

	for(unsigned int c = 0; c < (size - 1); c++)
		if(array[c] > array[c + 1])
			return false;

	return true;
}

template<typename T>
class Node
{
	public:

	T data;
	Node* next;

	Node(T element, Node* p)
	{data = element; next = p;}
};

template<typename T>
class LinkedList
{
	private:

	Node<T>* first;
	int mySize;

	public:

	LinkedList();
	~LinkedList();
	void insert(T element, int pos);
	T at(int pos);
};

template<typename T>
LinkedList<T>::LinkedList()
{
	first = NULL;
	mySize = 0;
}

template<typename T>
LinkedList<T>::~LinkedList()
{
	Node<T>* ptr = first;

	while(ptr != NULL){
		first = first->next;
		delete ptr;
		ptr = first;
	}

	mySize = 0;
}

template<typename T>
void LinkedList<T>::insert(T element, int pos)
{
	if(pos < 0 or pos > mySize) return;

	Node<T>* Inserting = new Node<T>(element, NULL);

	if(first == NULL){
		first = Inserting;
		mySize++;
		return;
	}
	
	if(pos == 0){
		Inserting->next = first;
		first = Inserting;
		mySize++;
		return;
	}
		
	if(pos == mySize){
		Node<T>* temp = first;
		int c = 0;
		while(c != mySize - 1){
			temp = temp->next;
			c++;
		}

		temp->next = Inserting;
		mySize++;
		return;
	}

	Node<T>* ptr = first;
	int counter = 0;

	while(counter != pos - 1){
		ptr = ptr->next;
		counter++;
	}

	Inserting->next = ptr->next;
	ptr->next = Inserting;
	mySize++;
}

template<typename T>
T LinkedList<T>::at(int pos)
{
	if(pos < 0 or pos >= mySize) return 0;
	//oh boy

	int counter = 0;

	Node<T>* ptr = first;

	while(counter != pos){
		ptr = ptr->next;
		counter++;
	}

	return ptr->data;
}

template<typename T1, typename T2>
class myPair
{
	public:
	
	T1 x1;
	T2 x2;
	
	myPair(T1 a, T2 b)
	{x1 = a; x2 = b;}
	
	bool operator>(myPair&);
};

template<typename T1, typename T2>
bool myPair<T1, T2>::operator > (myPair<T1, T2>& P)
{
	return (this->x1 > P.x1)? true : false;
}
	
int main()
{
	test_median();
	test_inOrder();
    test_at();
	test_myPair_operator();

	return 0;
}

void test_median()
{
	int A[] = {15, 1, 2, 7, 17};
	vector<int> V(A, A+5);
	cout << median(V) << endl;  // imprime 7

	double B[] = {15.1, 1.4, 2.3, 7.12, 17.99};
	vector<float> W(B, B+5);
	cout << median(W) << endl;    // imprime 7.12
}

void test_inOrder()
{
	int A[] = {15, 1, 2, 7, 17};
	int C[] = {1, 1, 2, 7, 17};
	cout << "inorder A:" << std::boolalpha << inOrder(A,5) << endl;
	cout << "inorder C:" << inOrder(C,5) << endl;

	double B[] = {15.1, 1.4, 2.3, 7.12, 17.99};
	cout << "inorder B:" <<   inOrder(B,5) << endl;

	string S[] = {"arroz", "habichuelas", "jamon" , "queso", "zetas"};
	cout << "inorder S:" << inOrder(S,5) << endl;
}

void test_at()
{
	LinkedList<string> L;
	L.insert("roble",0);
	L.insert("ucar",0);
	L.insert("ficus",2);

	cout << L.at(1) << endl;  // prints "roble"
}

void test_myPair_operator()
{
	myPair<int,string> P(8, "ocho");
	myPair<int,string> Q(10, "diez");
	cout << "Q is larger than P: " << std::boolalpha 
		 << (Q > P) << endl;  // should print "true"
}